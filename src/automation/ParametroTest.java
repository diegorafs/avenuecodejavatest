package automation;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.Test;

public class ParametroTest extends PaginaBase {

	protected PaginaBase paginaBase = new PaginaBase();;
	protected LoginPage loginPage;
	protected PaginaInicial paginaInicial;
	

	@Test
	public void valorParametro() throws Exception {
		taskInclusion();
		taskInclusionTwoFiveZero();
		taskInclusionLessThree();		
		subTaskInclusionTwoFiveZero();
		subTaskDueDate();
		closeBrowser();
		loginPage();
		userLogin();
	}

	//Loggin on main page
		private void loginPage() {
			this.paginaBase.navegateTo("http://qa-test.avenuecode.com/");
	    	}
		//User data
		private void userLogin() {
			this.loginPage = new LoginPage(this.paginaBase.getDriver());
			this.paginaInicial = loginPage.logar("diegorrabelo@outlook.com","kissband");
	   	}
		//Closing the browser
		private void closeBrowser() {
			this.paginaBase.closeNavigator();
		}

	//The task should require at least three characters so the user can enter it
	private void taskInclusionLessThree() {
		getDriver().findElement(By.id("new_task")).sendKeys("1");
		getDriver().findElement(By.xpath("//div[2]/span")).click();
		getDriver().findElement(By.id("new_task")).clear();
		getDriver().findElement(By.id("new_task")).sendKeys("12");
		getDriver().findElement(By.xpath("//div[2]/span")).click();
		getDriver().findElement(By.id("new_task")).clear();
		getDriver().findElement(By.id("new_task")).sendKeys("123");
	    getDriver().findElement(By.xpath("//div[2]/span")).click();
		
	}
	//The task can�t have more than 250 characters.
	private void subTaskInclusionTwoFiveZero() {
	getDriver().findElement(By.id("new_sub_task")).sendKeys("A Pol�cia Federal prendeu neste domingo (6), de "
			+ "forma preventiva, pelo menos 11 pessoas que faziam a prova do Enem (Exame Nacional do "
			+ "Ensino M�dio) com ponto eletr�nico, que permite fraude da prova. O n�mero de presos � "
			+ "resultado de duas opera��es da");
	getDriver().findElement(By.id("add-subtask")).click();
	getDriver().findElement(By.id("dueDate")).clear();
	
}
	
	private void taskInclusionTwoFiveZero() {
		getDriver().findElement(By.id("new_task")).sendKeys("A Pol�cia Federal prendeu neste domingo (6), "
				+ "de forma preventiva, pelo menos 11 pessoas que faziam a prova do Enem (Exame "
				+ "Nacional do Ensino M�dio) com ponto eletr�nico, que permite fraude da prova. O "
				+ "n�mero de presos � resultado de duas opera��es da PF");
		getDriver().findElement(By.xpath("//div[2]/span")).click();
		getDriver().findElement(By.id("new_task")).clear();
	}
	//Adding two task with the same name
	private void taskInclusion() {
		getDriver().findElement(By.id("new_task")).clear();
		getDriver().findElement(By.id("new_task")).sendKeys("Task01");
		getDriver().findElement(By.xpath("//div[2]/span")).click();
		getDriver().findElement(By.id("new_task")).clear();
		getDriver().findElement(By.id("new_task")).sendKeys("Task01");
		getDriver().findElement(By.id("new_task")).clear();
		
	}
	//There should be a form so you can enter the SubTask D
	//characters) and SubTask due date (MM/dd/yyyy format)
	//The user should click on the add button to add a new Sub
	//The Task Description and Due Date are required fields
	private void subTaskDueDate() {
		  driver.findElement(By.id("dueDate")).sendKeys("07/11/2016");
		  driver.findElement(By.id("add-subtask")).click();
		  driver.findElement(By.id("add-subtask")).click();
		
	}
}