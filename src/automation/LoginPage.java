package automation;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginPage extends PaginaBase {
	
	public LoginPage (WebDriver driver){
		super (driver);
	}
	public PaginaInicial logar (String typeUserName, String typePassword){
		realizaLogin (typeUserName, typePassword);
		return new PaginaInicial(getDriver());
	}

	private void realizaLogin (String typeUserName, String typePassword ){
		//driver.get(baseURL + "/users/sign_in");
		getDriver().findElement(By.linkText("Sign In")).click();
		getDriver().findElement(By.id("user_email"));
		getDriver().findElement(By.id("user_password"));
		getDriver().findElement(By.name("commit")).click();
		getDriver().findElement(By.xpath("(//a[contains(text(),'My Tasks')])[2]")).click();
	}

}
